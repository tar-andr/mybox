module.exports = {
    block: 'page',
    title: 'Пустая',
    content: [
        require('./common/header.bemjson'),
        require('./common/main.bemjson'),
        require('./common/footer.bemjson'),
    ],
};
