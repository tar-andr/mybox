const SOCIAL = [
    {
        href: '#',
        icon: 'fb',
    },
    {
        href: '#',
        icon: 'instagram',
    },
    {
        href: '#',
        icon: 'vk',
    },
    {
        href: '#',
        icon: 'ok',
    },
];

module.exports = [
  {block: 'footer', cls: 'bg-gray-800 text-white', content: [
      {cls: 'container', content: [
          {cls: 'row', content: [
              {cls: 'col-12 col-md-3 ', content: [
                  {block: 'a', cls: 'mt-2 d-inline-block', attrs: {href: '#'}, content: [
                      {block: 'img', mods: {lazy: true},
                          src: 'images/logo-white.png',
                          content: 'MYBOX',
                      },
                  ]},
              ]},
              {cls: 'col-12 col-md-6 mt-3 mt-md-0', content: [
                  {elem: 'list', cls: 'w-100', content: [
                      {cls: 'row', content: [
                          {cls: 'col-6 col-xl-4', content: [
                              {elem: 'item', cls: 'mb-md-2', content: [
                                  {elem: 'link', elemMods: {active: true}, attrs: {href: '#'}, content: 'Новости mybox'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', attrs: {href: '#'}, content: 'Обучение'},
                              ]},
                          ]},
                          {cls: 'col-6 col-xl-4', content: [
                              {elem: 'item', cls: 'mb-md-2', content: [
                                  {elem: 'link', attrs: {href: '#'}, content: 'Карта процессов'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', attrs: {href: '#'}, content: 'Маркетинг'},
                              ]},
                          ]},
                      ]},
                  ]},
              ]},
              {cls: 'col-12 col-md-3 mt-3 mt-md-0', content: [
                  {block: 'social', content: [
                      {block: 'title', mix: {block: 'social', elem: 'title'}, content: [
                          {elem: 'prefix', content: 'my'},
                          ' в соц сетях',
                      ]},
                      {elem: 'list', cls: 'mt-m mt-xl-xl', content: SOCIAL.map((index)=>[
                          {elem: 'item', content: [
                              {block: 'fi', mods: {icon: index.icon}},
                          ]},
                      ])},
                  ]},
              ]},
          ]},
          {cls: 'row mt-m mt-md-xl', content: [
              {cls: 'col-12 order-2 order-sm-1 col-sm-6 col-xl-9 pt-s', content: [
                  {elem: 'copyright', tag: 'span', content: '© 2013-2018'},
              ]},
              {cls: 'col-12 order-1 order-sm-2 col-sm-6 col-xl-3 pt-s text-gray-500', content: [
                  {elem: 'design', content: [
                      {tag: 'span', content: 'Дизайн сайта — '},
                      {elem: 'link', attrs: {
                          href: 'http://www.mazannikov.com',
                          target: '_blank',
                      }, content: [
                          {block: 'img', cls: 'align-baseline', src: 'images/mazannikov.png', content: 'www.mazannikov.com'},
                      ]},
                  ]},
              ]},
          ]},
      ]},
  ]},
  require('./modals.bemjson'),
];
