const ITEMS = 15;
const ACTIVE = 5;
module.exports = [
    {block: 'swiper-page-pagination', cls: 'd-flex mt-xxxl pt-1', attrs: {'data-active': ACTIVE}, content: [
        {elem: 'button', mods: {prev: true}, cls: 'swiper-button-disabled', content: [
            {block: 'a', cls: 'page-link', mix: {block: 'fi', mods: {icon: 'angle-left'}}},
        ]},
        {cls: 'swiper-container swiper-container-horizontal', content: [
            {block: 'pagination', cls: 'swiper-wrapper', content: [
                new Array(ITEMS).fill('').map((item, index) => [
                    {cls: index + 1 == 3 ? 'swiper-slide w-auto page-item active' : 'swiper-slide w-auto page-item', content: [
                        {block: 'a', cls: 'page-link active', content: index + 1},
                    ]},
                ]),
            ]},
        ]},
        {elem: 'button', mods: {next: true}, cls: 'swiper-button-disabled', content: [
            {block: 'a', cls: 'page-link', mix: {block: 'fi', mods: {icon: 'angle-right'}}},
        ]},
    ]},
];