module.exports = [
    {block: 'wall', content: [
        {elem: 'background', content: [
            {elem: 'img', width: '2080px', height: '594px', src: 'images/wall.jpg'},
        ]},
        {elem: 'inner', cls: 'show', content: [
            {cls: 'container h-100', content: [
                {elem: 'content', cls: 'text-white', content: [
                    {elem: 'title', content: 'welcome'},
                    {elem: 'subtitle', cls: 'mt-2', content: '-тренинг для новых сотрудников!'},
                ]},
            ]},
        ]},
    ]},
];
