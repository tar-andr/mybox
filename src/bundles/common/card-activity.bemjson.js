const ACTIVITY = [
    {
        img: 'images/img1.jpg',
        data: '04.11.18',
        title: 'WELCOME TO MYBOXTEAM',
        text: 'В конце каждого месяца ты можешь познакомиться с компанией поближе. Для этого просто нужно поучаствовать в Welcome-тренинге, где мы расскажем тебе обо всём.',
        link: '#',
    },
    {
        img: 'images/img2.jpg',
        data: '19.10.18',
        title: 'Игра для людей с хорошей фантазией',
        text: 'Первый игрок выдумывает несуществующее слово, а мы отгадываем его значение.',
        link: '#',
    },
    {
        img: 'images/img3.jpg',
        data: '04.11.18',
        title: 'А МЫ ТУТ СТАТИСТИКОЙ БАЛУЕМСЯ',
        text: 'Нет предела любопытству, захотелось нам узнать: где живут самые быстрые, куда приходят самые лояльные и что у наших гостей самое любимое!',
        link: '#',
    },
    {
        img: 'images/img4.jpg',
        data: '19.10.18',
        title: 'АСТРАХАНЬ!',
        text: 'Поздравляем с, безусловно, очень сложным и долгожданным открытием!',
        link: '#',
    },
];
module.exports = (item, index) => {
    item = item ? item : ACTIVITY[index];
    return [
        {block: 'card-activity', content: [
            {elem: 'img', content: [
                {block: 'image', mods: {size: '580x300'}, cls: 'text-center', content: [
                    {block: 'img', mods: {lazy: true}, src: item.img},
                ]},
            ]},
            {elem: 'body', content: [
                {elem: 'data', content: item.data},
                {elem: 'title', content: item.title},
                {elem: 'text', cls: 'mb-3', content: item.text},
                {elem: 'link', content: [
                    {tag: 'span', cls: 'align-middle', content: 'Подробнее'},
                    {block: 'fi', cls: 'align-middle ml-4', mods: {icon: 'arrow-long-right'}},
                ]},
            ]},
        ]},
    ];
};
