module.exports = [
    {block: 'main', content: [
        require('../common/wall.bemjson'),
        {cls: 'container', content: [
            {block: 'section', content: [
                {block: 'title', content: [
                    {elem: 'prefix', content: 'my'},
                    ' активность',
                ]},
                {cls: 'row', content: [...new Array(4)].map((item, index) => [
                    {cls: 'col-md-6 mt-4', content: [
                        require('./card-activity.bemjson')(false, index),
                    ]},
                ])},
                require('./swiper-page-pagination.bemjson'),
            ]},
            {block: 'section', cls: 'pb-l', content: [
                {block: 'title', content: [
                    {elem: 'prefix', content: 'my'},
                    ' вопросы и ответы',
                ]},
                {cls: 'row pb-1', content: [...new Array(4)].map((index)=>[
                    {cls: 'col-12', content: [
                        require('./faq.bemjson'),
                    ]},
                ])},
                require('./swiper-page-pagination.bemjson'),
            ]},
        ]},
    ]},
];
