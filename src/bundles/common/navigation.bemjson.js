const NAVIGATION = [
    {
        text: 'Новости mybox',
        link: '#',
    },
    {
        text: 'Карта процессов',
        link: '#',
    },
    {
        text: 'Обучение',
        link: '#',
    },
    {
        text: 'Маркетинг',
        link: '#',
    },
];

module.exports = [
    {block: 'navigation', content: [
        {cls: 'row', content: [
            {cls: 'col-12 col-md-10 col-lg-2', content: [
                {block: 'a', attrs: {href: '#'}, content: [
                    {block: 'img', mods: {lazy: true},
                        src: 'images/logo.png',
                        content: 'MYBOX',
                    },
                ]},
                {elem: 'btn', attrs: {
                    'data-toggle': 'collapse',
                    'data-target': '#menu-collapse',
                }, content: [...new Array(3)].map((item)=>[
                    {tag: 'span'},
                ])},
            ]},
            {cls: 'col-12 col-lg-8 position-static', content: [
                {elem: 'menu-wrapper', // mods: color
                        cls: 'navbar-expand-lg', content: [
                    {elem: 'menu', attrs: {id: 'menu-collapse'}, cls: 'collapse navbar-collapse', content: [
                        {elem: 'list', cls: 'navbar-nav', content: NAVIGATION.map((link, index)=>[
                            {elem: 'item', content: [
                                {elem: 'link', elemMods: index ? {active: false} : {active: true}, attrs: {href: link.link}, content: link.text},
                            ]},
                        ])},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
