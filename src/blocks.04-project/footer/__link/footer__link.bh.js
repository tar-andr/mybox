module.exports = (bh) => {
    bh.match('footer__link', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
        }, true);
    });
};
