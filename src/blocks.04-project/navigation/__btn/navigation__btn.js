/**
 * Created by 1 on 14.11.2018.
 */

const ClassName = {
    BLOCK: 'navigation',
    NAVIGATION: 'navigation__menu',
    WRAPPER: 'navigation__menu-wrapper',
    BTN: 'navigation__btn',
    CLOSE: 'navigation__btn_close',
    HIDDENBLOCK: 'wall__inner',
};

(($)=>{
        let $navigation = $(`.${ClassName.NAVIGATION}`);
        let $wrapper = $(`.${ClassName.WRAPPER}`);
        let $wall = $(`.${ClassName.HIDDENBLOCK}`);
        let $btn = $(`.${ClassName.BTN}`);
        let close = ClassName.CLOSE;
        $navigation.on('show.bs.collapse', (event)=>{
            $wrapper.show();
            $btn.addClass(close);
            $wall.removeClass('show');
        });
        $navigation.on('hide.bs.collapse', (event)=>{
            $wrapper.hide();
            $btn.removeClass(close);
            $wall.addClass('show');
        });
})($);
