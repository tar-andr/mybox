module.exports = (bh) => {
    bh.match('navigation__link', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
        }, true);
    });
};
