module.exports = (bh) => {
    bh.match('social__item', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
            target: '_blank',
        }, true);
    });
};
