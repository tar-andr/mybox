module.exports = (bh) => {
    bh.match('nav__link', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
        }, true);
    });
};
