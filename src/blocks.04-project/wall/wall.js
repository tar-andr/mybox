/**
 * Created by 1 on 13.11.2018.
 */

(($)=>{
    $('image').each((index, item)=>{
        let $this = $(item);
        $this.data('src') && $this.attr('xlink:href', $this.data('src'));
    });
})($);
