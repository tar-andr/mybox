module.exports = (bh) => {
    bh.match('card-activity__link', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
        }, true);
    });
};
